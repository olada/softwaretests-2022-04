package de.cofinpro.training.softwareengineering;

/**
 * Drückt aus, dass keine Bewertungen existieren.
 */
public class NoRatingsException extends Exception {
}
