<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>de.cofinpro.training.softwareengineering</groupId>
    <artifactId>softwaretests-2022-04</artifactId>
    <version>1.0.0-SNAPSHOT</version>

    <modules>
        <module>01-livecoding-testcycle</module>
        <module>02-aufgabe-unittests</module>
        <module>03-aufgabe-bowling</module>
        <module>04-livecoding-mockito</module>
        <module>05-placeholder</module>
        <module>06-livecoding-integrationtests</module>
        <module>07-cucumber</module>
    </modules>

    <packaging>pom</packaging>

    <properties>
        <slf4j.version>1.7.36</slf4j.version>
        <logback.version>1.2.11</logback.version>
        <lombok.version>1.18.22</lombok.version>

        <junit-jupiter.version>5.8.2</junit-jupiter.version>
        <mockito.version>4.5.0</mockito.version>
        <hamcrest.version>2.2</hamcrest.version>
        <assertj.version>3.22.0</assertj.version>
        <pitest.version>1.7.5</pitest.version>

        <maven-surefire-plugin.version>3.0.0-M6</maven-surefire-plugin.version>

        <java.version>11</java.version>
        <maven.compiler.target>${java.version}</maven.compiler.target>
        <maven.compiler.source>${java.version}</maven.compiler.source>

        <project.encoding>UTF-8</project.encoding>
        <project.build.sourceEncoding>${project.encoding}</project.build.sourceEncoding>
        <project.reporting.outputEncoding>${project.encoding}</project.reporting.outputEncoding>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-bom</artifactId>
                <version>${mockito.version}</version>
                <scope>import</scope>
                <type>pom</type>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>${logback.version}</version>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter</artifactId>
            <version>${junit-jupiter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-junit-jupiter</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-inline</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-library</artifactId>
            <version>${hamcrest.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <version>${assertj.version}</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${maven-surefire-plugin.version}</version>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>3.0.0-M6</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>integration-test</goal>
                        </goals>
                        <phase>test</phase>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>mutationTest</id>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-surefire-plugin</artifactId>
                        <version>${maven-surefire-plugin.version}</version>
                    </plugin>
                    <plugin>
                        <groupId>org.pitest</groupId>
                        <artifactId>pitest-maven</artifactId>
                        <version>${pitest.version}</version>
                        <dependencies>
                            <dependency>
                                <groupId>org.pitest</groupId>
                                <artifactId>pitest-junit5-plugin</artifactId>
                                <version>0.12</version>
                            </dependency>
                        </dependencies>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>mutationCoverage</goal>
                                </goals>
                                <phase>test</phase>
                            </execution>
                        </executions>
                        <configuration>
                            <mutators>
                                <!-- Siehe https://pitest.org/quickstart/mutators/ -->
                                <mutator>DEFAULTS</mutator>
                                <!--<mutator>STRONGER</mutator>-->
                                <!--<mutator>ALL</mutator>-->
                            </mutators>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <repositories>
        <repository>
            <id>central</id>
            <name>Maven Central</name>
            <url>https://repo1.maven.org/maven2/</url>
        </repository>
    </repositories>

    <pluginRepositories>
        <pluginRepository>
            <id>central</id>
            <name>Maven Central</name>
            <url>https://repo1.maven.org/maven2/</url>
        </pluginRepository>
    </pluginRepositories>

</project>