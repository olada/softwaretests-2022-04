package de.cofinpro.training.softwareengineering;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class MovieSteps {
    private final Map<String, Movie> moviesByTitle = new HashMap<>();
    private Movie currentMovie;

    @Given("movie {string}")
    public void movie(String movieTitle) {
        currentMovie = new Movie(movieTitle);
        moviesByTitle.putIfAbsent(movieTitle, currentMovie);
    }

    @And("has one/a rating of {int}")
    public void hasOneRatingOf(int rating) {
        currentMovie.addRating(rating);
    }

    @Given("movie {string} has one rating of {int}")
    public void movieHasOneRatingOf(String movieTitle, int rating) {
        currentMovie = new Movie(movieTitle);
        currentMovie.addRating(rating);
        moviesByTitle.putIfAbsent(movieTitle, currentMovie);
    }

    @When("the movie receives a new rating of {int}")
    public void theMovieReceivesANewRatingOf(int rating) {
        currentMovie.addRating(rating);
    }

    @When("movie {string} receives a new rating of {int}")
    public void movieReceivesANewRatingOf(String movieTitle, int rating) {
        Movie movie = moviesByTitle.get(movieTitle);
        movie.addRating(rating);
    }

    @Then("the average rating of the movie is {int}")
    public void theAverageRatingOfTheMovieIs(double averageRating) throws NoRatingsException {
        assertThat(currentMovie.getAverageRating()).isEqualTo(averageRating);
    }
}
