package de.cofinpro.training.softwareengineering;

import static de.cofinpro.training.softwareengineering.Action.RATED_MOVIE;

/**
 * Anwendung, die für einen Film mehrere Bewertungen entgegen nimmt und
 * eine Benutzeraktion protokolliert.
 */
public class Application {
    private MovieRepository movieRepository = new MovieRepository();
    private RatingService ratingService = new HttpRatingService();
    private UserService userService = new UserServiceImpl();

    public void run() {
        Movie shawshankRedemption = movieRepository.getMovieWithTitle("Shawshank Redemption");
        shawshankRedemption.addRating(ratingService, 3);
        shawshankRedemption.addRatings(ratingService, 5, 5, 5, 5, 5, 5, 5);

        userService.protocolAction(userService.getCurrentUser(), RATED_MOVIE);
    }
}
