package de.cofinpro.training.softwareengineering;

import lombok.extern.slf4j.Slf4j;

import java.util.Random;

/**
 * Einfache Implementierung eines User-Services.
 */
@Slf4j
public class UserServiceImpl implements UserService {
    private final Random random = new Random(System.currentTimeMillis());

    @Override
    public User getCurrentUser() {
        return new User(random.nextInt(), Runtime.getRuntime().toString());
    }

    @Override
    public void protocolAction(User user, Action action) {
        log.info("Logging action {} for user {}", action, user);
    }
}
