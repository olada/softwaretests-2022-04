package de.cofinpro.training.softwareengineering;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class MovieTest {
    // Dieser Mock ist für den dritten Test relevant
    @Mock
    private RatingService ratingService;

    @Test
    void should_add_rating_when_using_manual_mock() {
        RatingService ratingService = mock(RatingService.class);

        Movie movie = new Movie("The Godfather");
        movie.addRating(ratingService, 2);

        // "eq" notwendig, da wir mit "same" bereits einen Matcher als Argument nutzen
        verify(ratingService).addRating(same(movie), eq(2));
    }

    @Test
    void should_add_rating_when_using_parameter_mock(@Mock RatingService ratingService) {
        // Diese Zeile sparen wir uns dank des Parameters
        // RatingService ratingService = mock(RatingService.class);

        Movie movie = new Movie("The Godfather");
        movie.addRating(ratingService, 2);

        // "eq" notwendig, da wir mit "same" bereits einen Matcher als Argument nutzen
        verify(ratingService).addRating(same(movie), eq(2));
    }

    @Test
    void should_add_rating_when_using_field_mock() {
        // Diese Zeile sparen wir uns dank des Field-Mocks
        // RatingService ratingService = mock(RatingService.class);

        Movie movie = new Movie("The Godfather");
        movie.addRating(ratingService, 2);

        // "eq" notwendig, da wir mit "same" bereits einen Matcher als Argument nutzen
        verify(ratingService).addRating(same(movie), eq(2));
    }
}