package de.cofinpro.training.softwareengineering;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * Bewertungs-Service, der Bewertungen gegen eine fiktive HTTP-Schnittstelle
 * durchführt.
 */
@Slf4j
public class HttpRatingService implements RatingService {
    private static final String BASE_URI = "http://localhost";
    private static final int DEFAULT_PORT = 80;

    private final int port;

    public HttpRatingService() {
        this(DEFAULT_PORT);
    }

    public HttpRatingService(int port) {
        this.port = port;
    }

    private String getBaseUri() {
        return BASE_URI + ":" + port;
    }

    @Override
    public void addRating(Movie movie, int rating) {
        log.debug("Adding rating {} to movie with title '{}'", rating, movie.getTitle());

        try {
            HttpRequest request = createRequestForSingleRatingOperation(movie, rating);
            HttpClient httpClient = HttpClient.newHttpClient();
            httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (URISyntaxException | IOException | InterruptedException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    private HttpRequest createRequestForSingleRatingOperation(Movie movie, int rating) throws URISyntaxException {
        return HttpRequest.newBuilder()
                .uri(new URI(getBaseUri() + "/movie/" + encode(movie) + "/rate/" + rating))
                .PUT(HttpRequest.BodyPublishers.noBody())
                .build();
    }

    private String encode(Movie movie) {
        return URLEncoder.encode(movie.getTitle(), StandardCharsets.UTF_8).replace("+", "%20");
    }

    @Override
    public void addRatings(Movie movie, int... ratings) {
        Arrays.stream(ratings).forEach(rating -> addRating(movie, rating));
    }
}
