package de.cofinpro.training.softwareengineering;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;

class MovieRepositoryIT {
    private Connection connection;
    private MovieRepository movieRepository;

    @BeforeEach
    void beforeEach() throws SQLException, URISyntaxException {
        Path pathToSetupScript = Paths.get(getClass().getClassLoader().getResource("db-setup.sql").toURI());
        connection = DriverManager.getConnection("jdbc:h2:mem:;INIT=RUNSCRIPT FROM '" + pathToSetupScript + "'", "sa", "");
        movieRepository = new MovieRepository(connection);
    }

    @AfterEach
    void afterEach() throws SQLException {
        connection.close();
    }

    @Test
    void should_have_movie_starting_with_s_when_new_movie_is_added() throws SQLException {
        assertThat(movieRepository.getFirstMovieStartingWith("S"))
                .isEmpty();

        movieRepository.insert(new Movie("Shawshank Redemption"));

        assertThat(movieRepository.getFirstMovieStartingWith("S"))
                .hasValue(new Movie("Shawshank Redemption"));
    }
}
